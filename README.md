###使用说明

wc.py -choose [input_file_name]

-c 返回文件的字符数        -w 返回文件的单词总数

-l 返回文件的总行数        -s 递归处理目录下符合条件的文件

-a 返回注释行，代码行，空行数量   -e <stopList.txt> 返回单词总数(stopList.txt为停词表)

-o <outputfile.txt> 输出结果的文件 -h 获取帮助